import json
import socket
import sys
import math
import time

class Pis(object):
    def __init__(self, length=None, angle=None, radius=None, switch=False):
        self.length = length
        self.angle = angle
        self.radius = radius
        self.switch = switch

    def __str__(self):
        return "{} {} {} {}".format(self.length, self.angle, self.radius, self.switch)

class State(object):
    pass

class NoobBot(object):

    NUM_PISES = 4
    DECELERATE = 0.9804960054593885

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.pises = []
        self.positions = []
        self.lanes = []
        self.prv = None
        self.total = 0
        self.want_speed_index = 0
        self.tick = "???"
        self.turbo = False
        self.switching = False


    def on_game_init(self, data):
        track = data["race"]["track"]
        print "track is", track["id"], track["name"]

        self.track_id = track["id"]
        self.laps = data["race"]["raceSession"]["laps"]

        self.lanes = [None] * len(track["lanes"])
        for lane in track["lanes"]:
            self.lanes[lane["index"]] = lane["distanceFromCenter"]

        self.marks = []
        for i in xrange(len(self.lanes)):
            self.marks.append([])

        self.pises = []
        for pis in track["pieces"]:
            self.pises.append(Pis())
            if u"length" in pis: self.pises[-1].length = float(pis[u"length"])
            if u"radius" in pis: self.pises[-1].radius = float(pis[u"radius"])
            if u"angle" in pis: self.pises[-1].angle = float(pis[u"angle"])
            if u"switch" in pis: self.pises[-1].switch = True

            if self.pises[-1].radius:
                iterlanes = self.lanes if self.pises[-1].angle < 0 else self.lanes[::-1]
                self.pises[-1].length = [math.pi * (self.pises[-1].radius + lane_length) * abs(self.pises[-1].angle) / 180.0 for lane_length in iterlanes]
            else:
                self.pises[-1].length = [self.pises[-1].length] * len(self.lanes)

            print self.pises[-1]

        for i, pis1 in enumerate(self.pises):
            if pis1.switch:
                pis1.switch_length = [0] * len(self.lanes)
                for u in xrange(i+1, len(self.pises) * 2):
                    if self.pises[u % len(self.pises)].switch:
                        break
                    for t, l in enumerate(self.pises[u % len(self.pises)].length):
                        pis1.switch_length[t] += l

        self.lane_length = [0] * len(self.lanes)
        for pis in self.pises:
            for i, add in enumerate(pis.length):
                self.lane_length[i] += add

        limit = lambda x: (0.04 * x + 3.1) * 0.94

        #with open("{}.coeffs".format(self.track_id)) as coeffs:
        #    for line in coeffs:
        #        lane, dist, max_speed = line.split()
        #        lane = int(lane)
        #        dist = float(dist)
        #        max_speed = float(max_speed)
        #        #max_speed = 0.65

        #        self.marks[lane].append((dist, max_speed))

        for pis in self.pises:
            pis.dist_from_beginning = [0] * len(self.lanes)

        print 'lanes =',self.lanes
        for lane in xrange(len(self.lanes)):
            dist = 0
            for pis in self.pises:
                pis.dist_from_beginning[lane] = dist
                if not pis.radius:
                    self.marks[lane].append((dist, 18.3))
                    self.marks[lane].append((dist + pis.length[lane] - 5, 18.3))
                else:
                    #print 'pis.radius =', pis.radius, ' self.lanes[lane] =', self.lanes[lane]
                    #print 'calculating limit of', pis.radius + self.lanes[lane], pis.angle)
                    max_speed = limit(pis.radius - self.lanes[lane] * math.copysign(1, pis.angle))
                    self.marks[lane].append((dist, max_speed))
                    self.marks[lane].append((dist + pis.length[lane] - 5, max_speed))

                dist += pis.length[lane]

        for m in self.marks:
            m.sort()

        #print self.marks[0]
        #print self.marks[1]
        self.turbo_position = [-1] * len(self.lanes)
        TEST_LENGTH = 500
        for lane in xrange(len(self.lanes)):
            mx = 0

            front_pis = 0
            front_pos = 0

            rear_pis = 0
            rear_pos = 0

            good = 0.0

            total = 0.0
            while True:
                if total > self.lane_length[lane] + TEST_LENGTH + 20:
                    break

                front_pos += 10.0
                total += 10.0
                if front_pos >= self.pises[front_pis].length[lane] - 1e-8:
                    front_pis += 1
                    front_pis %= len(self.pises)
                    front_pos = 0.0

                if not self.pises[front_pis].radius:
                    good += 300
                else:
                    good += min(300, self.pises[front_pis].radius)

                if total > TEST_LENGTH:
                    if not self.pises[rear_pis].radius:
                        good -= 300
                    else:
                        good -= min(300, self.pises[rear_pis].radius)

                    rear_pos += 10.0
                    if rear_pos >= self.pises[rear_pis].length[lane] - 1e-8:
                        rear_pis += 1
                        rear_pis %= len(self.pises)
                        rear_pos = 0.0

                    if good > mx:
                        mx = good
                        self.turbo_position[lane] = self.pises[rear_pis].dist_from_beginning[lane] + rear_pos - 5

            
        print 'turbo_position =', self.turbo_position


    def estimate_throttle(self, cur):
        # odd number of pieces

        #straight = True
        #for pis in pises[self.NUM_PISES:]:
        #    if pis.radius:
        #        straight = False
        #        break
        #if straight:
        #    return 0.8
        #else:
        #    return 0.650

        cur_speed = cur.total - self.positions[-2].total

        cur_marks = self.marks[cur.lane]

        dist_from = self.pises[cur.pieceIndex].dist_from_beginning[cur.lane] +\
                    cur.position - cur_marks[self.want_speed_index][0]

        dist_between = cur_marks[(self.want_speed_index + 1) % len(cur_marks)][0] \
                     - cur_marks[self.want_speed_index][0]

        if self.want_speed_index + 1 == len(cur_marks):
            dist_between += self.lane_length[cur.lane]

        want_speed = cur_marks[self.want_speed_index][1] \
                   + (cur_marks[(self.want_speed_index + 1) % len(cur_marks)][1] \
                      - cur_marks[self.want_speed_index][1]) * dist_from / dist_between

        calc_throttle = lambda want_speed: (want_speed + 0.0637218) / 9.85285

        if cur_speed > 3.3:
            total = cur_marks[(self.want_speed_index + 1) % len(cur_marks)][0] - dist_from - cur_marks[self.want_speed_index][0]
            if self.want_speed_index + 1 == len(cur_marks):
                total += self.lane_length[cur.lane]

            print 'total =', total, cur_marks[(self.want_speed_index + 1) % len(cur_marks)]
            for w in xrange(1, 20):
                # estimate how much time do we have to speed down
                # have 'total' distance to slow down from 'cur_speed' to 'cur_marks[current][1]'
                current = (self.want_speed_index + w) % len(cur_marks)
                next = (current + 1) % len(cur_marks)

                target_speed = cur_marks[current][1]

                if target_speed < cur_speed - 0.002:
                    k = round(math.log(target_speed / cur_speed, self.DECELERATE))
                    # to slow down to target speed I need this distance:
                    distance = cur_speed * (self.DECELERATE * (1 - self.DECELERATE ** k) / (1 - self.DECELERATE))
                    if distance > total + 3.01: # for safety
                        print 'w =', w,\
                            'cur_mark[current] =', cur_marks[current], \
                            'total =', total
                        # I have a too long breaking distance
                        print 'break!'
                        return 0.0

                total += cur_marks[next][0] - cur_marks[current][0]
                if cur_marks[next][0] < cur_marks[current][0]:
                    if cur.lap == self.laps - 1:
                        # this is the new lap I will never see
                        break
                    total += self.lane_length[cur.lane]

        print "        lane =", self.positions[-1].lane, "dist_from =", dist_from, " dist_between =", dist_between, "want_speed =", want_speed
        return max(min(calc_throttle(want_speed + 7 * (want_speed - cur_speed)), 1), 0)

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        #self.join()
        #self.msg("createRace", {"botId": {"name": self.name,"key": self.key,},"trackName": "keimola","password": "romka","carCount": 2})
        self.msg("createRace", {"botId": {"name": self.name,"key": self.key,},"trackName": "france","password": "romka","carCount": 1})
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        for piece in data:
            if piece["id"]["name"] == self.name:
                cur = State()
                cur.pieceIndex = int(piece["piecePosition"]["pieceIndex"])
                cur.position = piece["piecePosition"]["inPieceDistance"]
                cur.lane = piece["piecePosition"]["lane"]["startLaneIndex"]
                cur.angle = piece["angle"]
                cur.lap = piece["piecePosition"]["lap"]
                cur.dist_from_start = cur.position + self.pises[cur.pieceIndex].dist_from_beginning[cur.lane]

                if self.pises[cur.pieceIndex].switch:
                    self.switching = False

                travelled = cur.position

                if self.prv != None:
                    travelled -= self.prv.position
                    if self.prv.pieceIndex != cur.pieceIndex:
                        travelled += self.pises[self.prv.pieceIndex].length[self.prv.lane]

                    cur.total = self.prv.total + travelled
                else:
                    cur.total = travelled

                self.prv = cur
                break

        self.positions.append(cur)
        if len(self.positions) == 1:
            self.positions.append(cur) # minimum two car positions to know the speed at once

        self.total += travelled

        if cur.dist_from_start < self.marks[cur.lane][self.want_speed_index][0]:
            self.want_speed_index = 0

        while self.want_speed_index + 1 < len(self.marks[cur.lane]) and self.marks[cur.lane][self.want_speed_index+1][0] < cur.dist_from_start:
            self.want_speed_index += 1

        throttle = self.estimate_throttle(cur)

        #throttle = 0.15 # for debug
        self.throttle(throttle)

        print "{:4}: {:7.3f} from start; speed = {:5.6f}; angle = {:6.3f}; {} {:20}".format(
                self.tick,
                cur.dist_from_start,
                self.positions[-1].total - self.positions[-2].total,
                cur.angle,
                self.want_speed_index,
                '#' * int(throttle * 20) + '.' * int((1 - throttle) * 20))

        dist1 = cur.dist_from_start - self.turbo_position[cur.lane]
        if dist1 < 0:
            dist1 += self.lane_length[cur.lane]
        if dist1 < 200 and self.turbo:
            self.activate_turbo()

        nxt = self.pises[(cur.pieceIndex + 1) % len(self.pises)]
        if nxt.switch and not self.switching:
            best = cur.lane
            mn = nxt.switch_length[cur.lane]
            for i, l in enumerate(nxt.switch_length):
                if abs(i - cur.lane) <= 1:
                    if l < mn and mn - l > 1e-7:
                        mn = l
                        best = i

            if best != cur.lane:
                self.switch(cur.lane, best)

    def switch(self, cur, need):
        return
        time.sleep(0.11)
        direction = "Left" if need < cur else "Right"
        self.msg("switchLane", direction)
        print "switch " + direction + "!"
        self.switching = True

    def activate_turbo(self):
        time.sleep(0.11)
        self.msg("turbo", "Pis!!!!!!")
        print "turbo!!"
        self.turbo = False

    def on_crash(self, data):
        print("Someone crashed")
        if self.name == data["name"]:
            print "Oh no no no!, it's me!"
            sys.exit(1)
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_receive_turbo(self, data):
        print "turboAvailable"
        self.turbo = True
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.on_game_init,
            'turboAvailable': self.on_receive_turbo,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if "gameTick" in msg:
                self.tick = int(msg["gameTick"])

            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
